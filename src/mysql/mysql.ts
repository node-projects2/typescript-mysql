import mysql = require('mysql');

export default class MySQL {

    private static _instance: MySQL;

    connection: mysql.Connection;
    conectado: boolean = false;

    private constructor() {
        console.log('Clase inicializada');
        this.connection = mysql.createConnection({
            host: 'localhost',
            user: 'root',
            password: '123456',
            database: 'nodejstest'
        });

        this.conectarDB();
    }

    public static get instance() {
        return this._instance || (this._instance = new this());
    }

    static ejecutarQuery(query: string, callback: Function) {
        this.instance.connection.query(query, (err, results: any[], fields) => {
            if (err) {
                console.log(err);
                return callback(err);
            }

            if (results.length === 0) {
                callback('El registro solicitado no existe');
                return;
            }
            callback(null, results);

        });
    }

    private conectarDB() {
        this.connection.connect((err: mysql.MysqlError) => {
            if (err) {
                console.log(err.message);
                return;
            }

            this.conectado = true;
            console.log('Base de datos levantada!');

        });
    }
}