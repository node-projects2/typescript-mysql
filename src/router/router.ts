import { Router, Request, Response } from 'express';
import MySQL from '../mysql/mysql';
import { MysqlError } from 'mysql';

const router = Router(); // crea una nueva instancia de nuestro router

router.get('/heroes', (req: Request, res: Response) => {
    const query = `SELECT * from heroes`;

    MySQL.ejecutarQuery(query, (err: MysqlError, heroes: any[]) => {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        } else {
            res.json({
                ok: true,
                heroes
            });
        }

    });
});

router.get('/heroes/:id', (req: Request, res: Response) => {
    const id = req.params.id;

    const escapeId = MySQL.instance.connection.escape(id);
    const query = `SELECT * from heroes where id=${escapeId}`;

    MySQL.ejecutarQuery(query, (err: MysqlError, heroe: any[]) => {
        if (err) {
            res.status(400).json({
                ok: false,
                error: err
            });
        } else {
            res.json({
                ok: true,
                heroe: heroe[0]
            });
        }

    });
});




export default router;