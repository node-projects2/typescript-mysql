// Se levanta apuntando con el comando 'nodemon dist/index' (esto va a buscar siempre el js)
// Para ver los cambios habrá que hacer el comando tsc antes para generar la compilación en el dist/

import Server from './server/server';
import router from './router/router';
import MySQL from './mysql/mysql';

const server = Server.init(3000);

server.app.use(router);

MySQL.instance;

server.start(() => {
    console.log(`Servidor corriendo en el puerto ${server.port}`);

});
